﻿using UnityEngine;
using System.Collections;

public class DamagePerso : MonoBehaviour {

    [SerializeField]
    private int life;

	public GameObject menu;

	public GameObject blood;

	void Start() 
	{
		life = 2;
	}

	public void Damage()
    {
		life--;
		StartCoroutine (takeDamage());
		if (life <= 0) 
		{
			menu.SetActive (true);
            GameObject.FindObjectOfType<Pause>().isPaused = true;
		}
    }
	private IEnumerator takeDamage() {
		blood.SetActive (true);
		yield return new WaitForSeconds (1.0f);
		blood.SetActive (false);
	} 
}
