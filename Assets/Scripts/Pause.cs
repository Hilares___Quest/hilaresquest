﻿    using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {


    static bool paused;

    // Use this for initialization
    void Start () {
        paused = false;

	}
	
	// Update is called once per frame
public void Update () {

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            paused = !paused;
        }

        if (paused)
        {
            Time.timeScale = 0;
        }

        if (!paused)
        {
            Time.timeScale = 1;
        }
    }

    public bool isPaused
    {
        set { paused = value;
            if (paused)
            {
                Time.timeScale = 0;
                Debug.Log("GamePaused");
            }
            else
            {
                Time.timeScale = 1;
                Debug.Log("GameUnpaused");
            }
                
             }
        get { return paused; }
    }

    //deprecated
    public void pause()
    {
        paused = !paused;
        if (paused)
        {
            Debug.Log("GamePaused");
            Time.timeScale = 0;

        }
        if (!paused)
        {
            Debug.Log("GameUnpaused");
            Time.timeScale = 1;

        }
    }   
}
