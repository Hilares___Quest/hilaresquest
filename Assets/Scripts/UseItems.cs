﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UseItems : MonoBehaviour {


    public Image Item;
    public GameObject FinalForm;
    public GameObject Original;
    public GameObject Buttons;

	
    void Start()
    {

        FinalForm.SetActive(false);
        Original.SetActive(true);
        Buttons.SetActive(false);
       
    }
	
   public  void OnMouseDown()
    {

        if (Item.isActiveAndEnabled)
        {
            Buttons.SetActive(true);
        }
    }
    

       public void Yes()
    {

        FinalForm.SetActive(true);
        Original.SetActive(false);
        Item.enabled = false;
        Buttons.SetActive(false);

    }

    public void No()
    {
        FinalForm.SetActive(false);
        Original.SetActive(true);
        Item.enabled = true;
        Buttons.SetActive(false);
    }
}
