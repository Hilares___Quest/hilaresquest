﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class WaypointNetworkManager : MonoBehaviour {

    [SerializeField]
    private GameObject m_activeNetwork;

    public delegate void UpdateAll(GameObject newNetwork);

    [SerializeField]
    private event UpdateAll m_call;
    //private List<Action<GameObject>> m_call;


    public GameObject activenetwork
    {
        set { m_activeNetwork = value; m_call(m_activeNetwork); }
        get { return m_activeNetwork; }
    }

#if UNITY_EDITOR

    private GameObject m_last;

    // Update is called once per frame
    void Update()
    {
        if (m_activeNetwork != m_last)
        {
            m_call(m_activeNetwork);
            m_last = m_activeNetwork;
        }
    }
#endif 

    // Use this for initialization
    void Start () {
	    
	}
	
	

    public void AddEvent(UpdateAll function)
    {
        //m_call.Add(function);
        m_call += function;
    }

    //private void UpdateAll()
    //{
    //    for (int i = 0; i < m_call.Count; i++)
    //    {
    //        m_call[i](m_activeNetwork);
    //    }
    //}
}
