﻿using UnityEngine;
using System.Collections;

public class OpenFichePersonnage : MonoBehaviour {

    public bool Clicked;
    public GameObject PersPage;
    public GameObject OtherPage1;
    public GameObject OtherPage2;
	public GameObject OtherPage3;

    /*public GameObject OtherIcone;
    public GameObject OtherIcone2;
    public GameObject OtherIcone3;*/

	public GameObject cam;

	private Pause pausing;

    [SerializeField]
    private OpenFichePersonnage[] pageMgr;

    void Start()
    {

        Clicked = false;
        PersPage.SetActive(false);
        pausing = cam.GetComponent<Pause>();
        //cam = GameObject.FindGameObjectWithTag("MainCamera");

    }

    public void IconeClicked()
    {
        Clicked = !Clicked;
        if (Clicked)
        {
            PersPage.SetActive(true);
            //OtherPage1.GetComponent<OpenFichePersonnage>().Clicked = false;
            OtherPage1.SetActive(false);
            //OtherPage2.GetComponent<OpenFichePersonnage>().Clicked = false;
            OtherPage2.SetActive(false);
            //OtherPage3.GetComponent<OpenFichePersonnage>().Clicked = false;
            OtherPage3.SetActive (false);

            for (int i = 0; i < pageMgr.Length; i++)
            {
                pageMgr[i].Clicked = false;
            }
            /*OtherIcone.SetActive(false);
            OtherIcone2.SetActive(false);
            OtherIcone3.SetActive(false);*/

            
			//pausing.pause ();
            pausing.isPaused = true;
        }
        if (!Clicked)
        {
            PersPage.SetActive(false);
            /*OtherIcone.SetActive(true);
            OtherIcone2.SetActive(true);
            OtherIcone3.SetActive(true  );*/
            //pausing.pause ();
            pausing.isPaused = false;
        }
    }
}
