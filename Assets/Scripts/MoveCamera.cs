﻿using UnityEngine;
using System.Collections;

public class MoveCamera : MonoBehaviour
{
    int Delta = 50;
    float Move = 0.7f;
    public GameObject Personnages;
    public GameObject mainCamera;

    void Update()
    {
			//Control la caméra
			if (Input.GetKey (KeyCode.Z) && Input.mousePosition.x >= 0 && Input.mousePosition.x <= Screen.width && Input.mousePosition.y >= 0 && Input.mousePosition.y <= Screen.height) {
				//Souris à droite de l'écran
	            if (Input.mousePosition.x >= Screen.width - Delta) 
				{
					//Bloqué au bord de l'écran
					if (mainCamera.transform.position.x >= 400) 
					{
						transform.Translate (0, 0, 0);
						//Décalage vers la droite
					}
					else 
					{
						transform.Translate (Move, 0, 0);
					}
	                
				}
				//Souris à gauche de l'écran
	            else if (Input.mousePosition.x <= Delta) 
				{
					//Bloqué au bord de l'écran
					if (mainCamera.transform.position.x <= -100) 
					{
						transform.Translate (0, 0, 0);
					}
					//Décalage vers la gauche
	                else 
					{
						transform.Translate (-Move, 0, 0);
					}
				}
			}
			//Suit le groupe
			else 
			{
				Vector3 cameraPosition = new Vector3 (Personnages.transform.position.x+10, 17.3f, 77.7f);
				mainCamera.transform.position = cameraPosition;
			}
    }
}
