﻿using UnityEngine;
using System.Collections;

public class SoldierAttack : MonoBehaviour {

	public GameObject Camera;

	private DamagePerso Damage;

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("team")) {
			Debug.Log ("Yatta");
			Damage = Camera.GetComponent<DamagePerso>();
			Damage.Damage();
			Damage.Damage();
		}
	}
}
