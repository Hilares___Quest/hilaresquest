﻿using UnityEngine;
using System.Collections;

public class CharactereMovement : MonoBehaviour {

    public Animator Player;

    private Vector3 Pos;

    void Start()
    {
        Pos = this.transform.position;
    }

    void Update()
    {
        if (this.transform.position == Pos)
        {
            Player.SetBool("Walking", false);
        }
        else
        {
            Player.SetBool("Walking", true);
        }
        Pos = this.transform.position;
    }
}
