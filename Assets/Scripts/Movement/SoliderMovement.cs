﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class SoliderMovement : MonoBehaviour {

	public GameObject team;
	public GameObject soldier1;
	public GameObject soldier2;
	public GameObject soldier3;

	private AI AI;

	void OnTriggerEnter() {
		AI = soldier1.GetComponentInChildren<AIRig>().AI;
		AI.WorkingMemory.SetItem<GameObject>("target", team);

		AI = soldier2.GetComponentInChildren<AIRig>().AI;
		AI.WorkingMemory.SetItem<GameObject>("target", team);

		AI = soldier2.GetComponentInChildren<AIRig>().AI;
		AI.WorkingMemory.SetItem<GameObject>("target", team);
	}
}
