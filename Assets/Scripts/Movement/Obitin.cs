﻿using UnityEngine;
using System.Collections;

public class Obitin : MonoBehaviour {

	public GameObject obitinCharge;
	public GameObject obitinMove;
	public GameObject obitinIcone;

	void OnTriggerEnter(Collider other) {
		obitinCharge.SetActive (false);
		obitinMove.SetActive (true);
		obitinIcone.SetActive (true);
	}
}
