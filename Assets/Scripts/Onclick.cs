﻿using UnityEngine;
using System.Collections;

public class Onclick : MonoBehaviour {

    public GameObject activated1;
    public GameObject activated2;
    public GameObject desactivated1;
    public GameObject desactivated2;

    [SerializeField]
    private Animator Taverne;

    void OnMouseDown()
    {
        Taverne.SetBool("click", true);
        desactivated1.SetActive(false);
        desactivated2.SetActive(false);
        activated1.SetActive(true);
        activated2.SetActive(true);
    }
}
