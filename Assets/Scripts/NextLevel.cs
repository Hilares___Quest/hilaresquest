﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class NextLevel : MonoBehaviour {

	public GameObject NextPathNetwork;
	public GameObject Team;

	private AI AI;

    void OnTriggerEnter (Collider Other)
    {
        //Set next path
		AI = Team.GetComponentInChildren<AIRig>().AI;
		AI.GetCustomElement<TeamAI>().pathNetwork = NextPathNetwork;
    }
}
