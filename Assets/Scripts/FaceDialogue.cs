﻿using UnityEngine;
using System.Collections;

public class FaceDialogue : MonoBehaviour {

	public GameObject TaupeFace;
	public GameObject ObitinFace;
	public GameObject BardabeFace;
	public GameObject GardeFace;

	public void chooseFace(string Face)
	{
		if (Face == "lines") {
			TaupeFace.SetActive (false);
			ObitinFace.SetActive (false);
			BardabeFace.SetActive (false);
			GardeFace.SetActive (false);
		} else if (Face == "bardabe") {
			TaupeFace.SetActive (false);
			ObitinFace.SetActive (false);
			BardabeFace.SetActive (true);
			GardeFace.SetActive (false);
		} else if (Face == "taupe") {
			TaupeFace.SetActive (true);
			ObitinFace.SetActive (false);
			BardabeFace.SetActive (false);
			GardeFace.SetActive (false);
		} else if (Face == "obitin") {
			TaupeFace.SetActive (false);
			ObitinFace.SetActive (true);
			BardabeFace.SetActive (false);
			GardeFace.SetActive (false);
		} else if (Face == "garde") {
			TaupeFace.SetActive (false);
			ObitinFace.SetActive (false);
			BardabeFace.SetActive (false);
			GardeFace.SetActive (true);
		} else {
			TaupeFace.SetActive (false);
			ObitinFace.SetActive (false);
			BardabeFace.SetActive (false);
			GardeFace.SetActive (false);
		}

	}
}
