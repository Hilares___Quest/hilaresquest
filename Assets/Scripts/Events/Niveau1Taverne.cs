﻿using UnityEngine;
using System.Collections;

public class Niveau1Taverne : MonoBehaviour {

	public GameObject Level4WithMoney;
	public GameObject Level4WithoutMoney;

	public GameObject dialogueWithMoney;
	public GameObject dialogueWithoutMoney;

	void OnMouseDown() 
	{
		Level4WithMoney.SetActive (true);
		Level4WithoutMoney.SetActive (false);

		dialogueWithMoney.SetActive (true);
		dialogueWithoutMoney.SetActive (false);
	}
}
