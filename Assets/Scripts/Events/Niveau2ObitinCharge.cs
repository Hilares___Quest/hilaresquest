﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class Niveau2ObitinCharge : MonoBehaviour {

	//Charge
	public Animator ObitinCharge;

	//Rain
	public GameObject Target;
	public GameObject Character;

	//Hp
	public GameObject Camera;

	//Eviter le coup 
	public GameObject Avis;

	//Déclarations locals
	private AI AI;
	private DamagePerso Damage;
	private Poster PosterStatus;

	void OnTriggerStay()
	{
		//lance l'animation de charge 
		ObitinCharge.SetBool ("Trigger", true);
		//Set target
		Debug.Log("Bardabé");
		AI = Character.GetComponentInChildren<AIRig>().AI;
		AI.WorkingMemory.SetItem<GameObject>("target", Target);
		//Check si le poster a été vu 
		PosterStatus = Avis.GetComponent<Poster>();
		if (PosterStatus.PosterVisited == false) 
		{
            Debug.Log("if");
			//-1 hp
			StartCoroutine (takeDamage());
		}

        this.GetComponent<SphereCollider>().enabled = false;
    }
	private IEnumerator takeDamage() {
		yield return new WaitForSeconds (1.0f);
		Damage = Camera.GetComponent<DamagePerso>();
		Damage.Damage();
	} 

	public void Continu()
	{
		//delete target
		AI = Character.GetComponentInChildren<AIRig>().AI;
		AI.WorkingMemory.SetItem<GameObject>("target", null);
	}
}
