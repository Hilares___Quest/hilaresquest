﻿using UnityEngine;
using System.Collections;

public class TonneauStop : MonoBehaviour {

	public GameObject Team;
	public GameObject garde1;
	public GameObject garde2;
	public GameObject garde3;

    private DamagePerso Damage;

    public GameObject Camera;

    void OnTriggerEnter(Collider other) 
	{
		Debug.Log ("SomethingEnter");
		if (other.CompareTag("team")) 
		{
            Damage = Camera.GetComponent<DamagePerso>();
            Damage.Damage();
            Damage.Damage();
            Debug.Log ("teamEnter");
			//Team.GetComponentInChildren<AIRig>();
			Team.SetActive(false);
		} 
		else if (other.CompareTag ("garde")) 
		{
			Debug.Log ("garde");
			garde1.SetActive (false);
			garde2.SetActive (false);
			garde3.SetActive (false);
		}
	}
}