﻿using UnityEngine;
using System.Collections;

public class ActivateEventTrigger : MonoBehaviour {

    public GameObject EventTrigger;

	void OnMouseDown()
    {
        EventTrigger.SetActive(true);
    }
}
