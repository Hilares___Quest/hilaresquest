﻿using UnityEngine;
using System.Collections;

public class StopBottleGarde : MonoBehaviour {

    public GameObject BottleGarde;
	public GameObject OtherGarde;

    private Transform BottleGardeChild;
    private Transform OtherGardeChild;


        void OnMouseDown()
        {
        BottleGardeChild = BottleGarde.transform.FindChild("Destroyer");
        BottleGardeChild.gameObject.SetActive(false);

        OtherGardeChild = OtherGarde.transform.FindChild("Destroyer");
        OtherGardeChild.gameObject.SetActive(false);
    }
}
