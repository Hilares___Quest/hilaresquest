﻿using UnityEngine;
using System.Collections;

public class EventTonneaux : MonoBehaviour {
    public GameObject Tonneau;
    public GameObject TonneauEnFeu;
	public GameObject Restart;

	public GameObject hilight;
	public GameObject explosion;

    [SerializeField]
    private GameObject[] Gardes;

    void OnMouseDown()
    {
		Restart.SetActive (true);
        Tonneau.SetActive(false);
        TonneauEnFeu.SetActive(true);
		hilight.SetActive (false);
		explosion.SetActive (true);

        for (int i = 0; i < Gardes.Length; i++)
        {
            Gardes[i].SetActive(false);
        }
    }

}
