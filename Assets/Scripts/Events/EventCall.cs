﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class EventCall : MonoBehaviour {

    public GameObject Target;
    public GameObject Character;

    private AI AI;

    void OnTriggerStay()
    {
        //Set target
        AI = Character.GetComponentInChildren<AIRig>().AI;
        AI.WorkingMemory.SetItem<GameObject>("target", Target);
        this.GetComponent<SphereCollider>().enabled = false;
    }
    public void Continu()
    {
		//delete target
        AI = Character.GetComponentInChildren<AIRig>().AI;
        AI.WorkingMemory.SetItem<GameObject>("target", null);
    }

}
