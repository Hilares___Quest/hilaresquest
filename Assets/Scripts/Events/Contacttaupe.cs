﻿using UnityEngine;
using System.Collections;

public class Contacttaupe : MonoBehaviour {

    public bool Argent;
    public Animator TestNiveau4;

    void OnTriggerEnter(Collider other)
    {
        if (Argent)
        {
            TestNiveau4.SetBool("Argent", true);
        }
        else
        {
            TestNiveau4.SetBool("Argent", false);
        }
    }
}
