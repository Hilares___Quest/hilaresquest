﻿using UnityEngine;
using System.Collections;

public class ClickOnProps : MonoBehaviour {

    public GameObject Event;
    public GameObject Border;
    //public GameObject Props;

	public GameObject  red;
	public GameObject  green;

    public void IsClicked()
    {
        Border.SetActive(true);
        Event.SetActive(true);
		red.SetActive (false);
		green.SetActive (true);
    }
}
