﻿using UnityEngine;
using System.Collections;

public class Poster : MonoBehaviour {

	public Animator ObitinCharge;
	public bool PosterVisited;

	void Start() 
	{
		PosterVisited = false;
	}

	void OnMouseDown()
	{
		ObitinCharge.SetBool ("Poster", true);
		PosterVisited = true;
	}
		
}
