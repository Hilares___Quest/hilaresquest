﻿using UnityEngine;
using System.Collections;

public class BottleGardeAttaque : MonoBehaviour {

	//Hp
	public GameObject Camera;

	private DamagePerso Damage;

	void OnTriggerEnter() 
	{
		//-1 hp
		Damage = Camera.GetComponent<DamagePerso>();
		Damage.Damage();
        this.GetComponent<SphereCollider>().enabled = false;
    }
}
