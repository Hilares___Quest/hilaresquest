﻿using UnityEngine;
using System.Collections;

public class ChooseSpeec : MonoBehaviour {

    public GameObject Dialogue1;
    public GameObject Dialogue2;
    public GameObject Dialogue3;

    void Start () {
        Dialogue1.SetActive(false);
        Dialogue2.SetActive(false);
        Dialogue3.SetActive(false);
	
	}
	
	
	public void FirstButton()
    {
        Dialogue1.SetActive(true);
        Dialogue2.SetActive(false);
        Dialogue3.SetActive(false);
    }

    public void SecondButton()
    {
        Dialogue1.SetActive(false);
        Dialogue2.SetActive(true);
        Dialogue3.SetActive(false);
    }

    public void ThirdButton()
    {
        Dialogue1.SetActive(false);
        Dialogue2.SetActive(false);
        Dialogue3.SetActive(true);
    }
}
