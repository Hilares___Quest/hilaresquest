﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SecondDialogManager : MonoBehaviour {

    public GameObject TextBox;
    public Text theText;

    public TextAsset textFile;
    public string[] TextLines;

    public int CurrentLines;
    public int endAtLines;

    public float Textmovement = 5;

    public GameObject Bubble;
    public bool isActive;

     
    



    void Start()
    {

        Bubble.SetActive(false);

        if (textFile != null)
        {
            TextLines = (textFile.text.Split('\n'));
        }


        if (endAtLines == 0)
        {
            endAtLines = TextLines.Length - 1;

        }
    }

    public void Update()
    {
        if (Bubble.activeSelf) {
            Textmovement -= Time.deltaTime;


            if (CurrentLines < endAtLines)
            {
                theText.text = TextLines[CurrentLines];
            }
            

            if (Textmovement <= 0)
            {
                CurrentLines++;
                Textmovement = 3;
            }



            if (CurrentLines >= endAtLines)
            {
                TextBox.SetActive(false);
            }
        }
    }


    public void Startit()
    {

        if (!isActive)
        {
            Bubble.SetActive(true);


        }
    }

}
