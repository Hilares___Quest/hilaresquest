﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThirdDialogueManager : MonoBehaviour {

    public GameObject TextBox;
    public Text theText;

    public TextAsset textFile;
    public string[] TextLines;

    public int CurrentLines;
    public int endAtLines;


    void Start()
    {



        if (textFile != null)
        {
            TextLines = (textFile.text.Split('\n'));
        }


        if (endAtLines == 0)
        {
            endAtLines = TextLines.Length - 1;

        }
    }

    void Update()
    {
        if (CurrentLines < endAtLines)
        {
            theText.text = TextLines[CurrentLines];
        }

        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            CurrentLines++;
        }

        if (CurrentLines >= endAtLines)
        {
            TextBox.SetActive(false);
        }
    }

}
