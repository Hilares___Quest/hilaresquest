﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogBoxSecondCharacter : MonoBehaviour {

  void Update() { 
        StartCoroutine(DoFade());
    }

    IEnumerator DoFade()
    {
        CanvasGroup Conversation = GetComponent<CanvasGroup>();
        if (Conversation.alpha > 0)
        {
            Conversation.alpha -= Time.deltaTime / 2;
            yield return null;
        }

        else if(Conversation.alpha == 0)
        {
            Conversation.alpha += Time.deltaTime * 2;
            yield return null;
        }
         
        Conversation.interactable = false;
        yield return null;
    }

 
}
