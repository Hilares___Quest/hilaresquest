﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class DialogBoxManager : MonoBehaviour {

    public GameObject Event;
    private EventCall next;

    public GameObject TextBox;
    public Text theText;

    public TextAsset textFile;
    public string[] TextLines;

    public int CurrentLines;
    public int endAtLines;

	//faces 
	public string face1;
	public string face2;
	public string face3;
	public string face4;
	public string face5;
	public string face6;
	public string face7;

	public GameObject Manager;

	private FaceDialogue choose;

   // [SerializeField]
    //private Animator prout;


    void Start () {
        if(textFile != null)
        {
            TextLines = (textFile.text.Split('\n'));
        }


        if (endAtLines == 0)
        {
            endAtLines = TextLines.Length - 1;
        }
    }

	void ChooseFace() {
		choose = Manager.GetComponent<FaceDialogue>();
		if (CurrentLines == 0) {
			choose.chooseFace (face1);
		}
		if (CurrentLines == 1) {
			choose.chooseFace (face2);
		}
		if (CurrentLines == 2) {
			choose.chooseFace (face3);
		}
		if (CurrentLines == 3) {
			choose.chooseFace (face4);
		}
		if (CurrentLines == 4) {
			choose.chooseFace (face5);
		}
		if (CurrentLines == 5) {
			choose.chooseFace (face6);
		}
		if (CurrentLines == 6) {
			choose.chooseFace (face7);
		}
	}

    void Update()
    {
        if (CurrentLines < endAtLines)
        {
            theText.text = TextLines[CurrentLines];
			ChooseFace ();
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            CurrentLines ++;
        }

        if(CurrentLines >= endAtLines)
        {
            //prout.SetBool("Go", true);
            Debug.Log("Next");
            next = Event.GetComponentInChildren<EventCall>();
            next.Continu();
            TextBox.SetActive(false);
        }
    }	
}
