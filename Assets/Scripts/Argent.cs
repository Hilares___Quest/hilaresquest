﻿using UnityEngine;
using System.Collections;

public class Argent : MonoBehaviour {

	public GameObject argent;

	public Animator TavernePNJ;

	void OnTriggerEnter(Collider other){
		argent.SetActive (true);
		TavernePNJ.SetBool ("click", true);
	}
}
