﻿using UnityEngine;
using System.Collections;

public class TriggerP1 : MonoBehaviour {

    public GameObject phrase;
    public GameObject GameManagement;
	public string Face;

	public GameObject Manager;

	private FaceDialogue choose;

    void OnTriggerEnter(Collider other)
    {
        GameManagement.GetComponent<Erase>();
        phrase.SetActive(true);
		choose = Manager.GetComponent<FaceDialogue>();
		choose.chooseFace(Face);
        Destroy(this.gameObject);
    }
}
