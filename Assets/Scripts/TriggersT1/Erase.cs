﻿using UnityEngine;
using System.Collections;

public class Erase : MonoBehaviour {

    public GameObject P1;
    public GameObject P2;
    public GameObject P3;
    public GameObject P4;
    public GameObject P5;
    public GameObject P6;
    public GameObject P7;
    public GameObject P8;
    public GameObject P9;
    public GameObject P10;
    public GameObject P11;
    public GameObject P12;
    public GameObject P13;
    public GameObject P14;

    public GameObject D1;
    public GameObject D2;
	public GameObject D3;

    void Start () {
        P1.SetActive(false);
        P2.SetActive(false);
        P3.SetActive(false);
        P4.SetActive(false);
        P5.SetActive(false);
        P6.SetActive(false);
        P7.SetActive(false);
        P8.SetActive(false);
        P9.SetActive(false);
        P10.SetActive(false);
        P11.SetActive(false);
        P12.SetActive(false);
        P13.SetActive(false);
        P14.SetActive(false);

        D1.SetActive(false);
        D2.SetActive(false);
		D3.SetActive (false);
    }

}
