﻿using UnityEngine;
using System.Collections;

public class RotateCamera : MonoBehaviour
{
    public GameObject youranimatableObject;
    private Animator yourAnimator;
    public bool HasTurnedtoCredits;
    public bool HasTurned;

    void Start()
    {
        yourAnimator = youranimatableObject.GetComponent<Animator>();
        HasTurnedtoCredits = false;
        HasTurned = false;
    }

   public void Controles()
    {
     
            HasTurnedtoCredits = true;
            if(HasTurnedtoCredits == true)
            {
                yourAnimator.SetBool("Haspressed", true);
            }
        

    }

    public void RetourversMainMenu()
    {

        HasTurnedtoCredits = false;
        if (HasTurnedtoCredits == false)
        {
            yourAnimator.SetBool("Haspressed", false);
        }

    }

    public void Credits()
    {
        HasTurned = true;
        if(HasTurned == true)
        {
            yourAnimator.SetBool("TotheLeft", true);
        }
    }

    public void GoBacktoMenu()
    {
        HasTurned = false;
        if (HasTurned == false)
        {
            yourAnimator.SetBool("TotheLeft", false);
        }
    }
}