﻿using UnityEngine;
using System.Collections.Generic;
using RAIN.Core;
using RAIN.Serialization;

[RAINSerializableClass]
public class TeamAI : CustomAIElement
{
    //[RAINSerializableField]
    //private float _standardFloat = 1.2f;

    [RAINSerializableField(Visibility = FieldVisibility.ShowAdvanced, ToolTip = "An advanced float")]
    private float _advancedFloat = 4.5f;

    [RAINSerializableField]
    private List<GameObject> m_characters;

    [RAINSerializableField]
    private List<GameObject> m_charactersLocations;

    [RAINSerializableField]
    private GameObject m_network;

    private List<bool> m_isInTeam;

    bool m_isFull;

    public GameObject pathNetwork
    {
        set
        {
            m_network = value;
            AI.WorkingMemory.SetItem<GameObject>("Network", value);
            UpdatePathNetwork();

        }
        get
        {
            return m_network;
        }
    }

    public override void AIInit()
    {
        base.AIInit();

        // This is equivilent to an Awake call

        m_isInTeam = new List<bool>();

        for (int i = 0; i < m_characters.Count; i++)
        {
            m_isInTeam.Add(false);
        }

        //m_isFull = AI.WorkingMemory.GetItem<bool>("IsFull");



        AI.WorkingMemory.SetItem<GameObject>("Network", m_network);
        UpdatePathNetwork();
    }

    public override void Pre()
    {
        base.Pre();

        for (int i = 0; i < m_characters.Count; i++)
        {
            if (m_characters[i] != null)
            {
                m_characters[i].GetComponentInChildren<AIRig>().AI.GetCustomElement<PlayerCharacter>().team = AI.Body;
                m_characters[i].GetComponentInChildren<AIRig>().AI.GetCustomElement<PlayerCharacter>().characterId = i;
                if (m_charactersLocations.Count > i)
                    m_characters[i].GetComponentInChildren<AIRig>().AI.GetCustomElement<PlayerCharacter>().positionInTeam = m_charactersLocations[i];
            }
        }

    }

    public override void Act()
    {
        base.Act();

#if UNITY_EDITOR
        if (AI.WorkingMemory.GetItem<GameObject>("Network") != m_network)
        {
            AI.WorkingMemory.SetItem<GameObject>("Network", m_network);
            UpdatePathNetwork();
        }
#endif
    }

    private void UpdatePathNetwork()
    {
        AI.WorkingMemory.SetItem<bool>("switchBreak", true);
    }

    public void JoinTeam(int id)
    {
        Debug.Log(m_isInTeam.Count);
        if(id > -1 && id < m_isInTeam.Count)
        {
            m_isInTeam[id] = true;
        }
        AI.WorkingMemory.SetItem("IsFull", true);

        for (int i = 0; i < m_isInTeam.Count; i++)
        {
            if (m_isInTeam[i] == false)
            {
                AI.WorkingMemory.SetItem("IsFull", false);
                Debug.Log("setFalse");
            }
                
        }
    }

    public void LeaveTeam(int id)
    {
        if (id > -1 && id < m_isInTeam.Count)
        {
            m_isInTeam[id] = false;
        }


        AI.WorkingMemory.SetItem("IsFull", false);
    }



}