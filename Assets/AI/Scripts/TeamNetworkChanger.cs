﻿using RAIN.Core;
using RAIN.Serialization;
using UnityEngine;

[RAINSerializableClass]
public class TeamNetworkChanger : NetworkChanger
{
    public override void SetNetwork(GameObject newNetwork)
    {
        base.SetNetwork(newNetwork);

        AI.GetCustomElement<TeamAI>().pathNetwork = newNetwork;
    }
}