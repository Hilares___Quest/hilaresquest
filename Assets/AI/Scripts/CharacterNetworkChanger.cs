﻿using RAIN.Core;
using RAIN.Serialization;
using UnityEngine;

[RAINSerializableClass]
public class CharacterNetworkChanger : NetworkChanger
{
    public override void SetNetwork(GameObject newNetwork)
    {
        base.SetNetwork(newNetwork);

        AI.WorkingMemory.SetItem(_networkNameValue, newNetwork);
    }
}