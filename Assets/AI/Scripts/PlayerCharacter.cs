﻿using RAIN.Core;
using RAIN.Serialization;
using UnityEngine;

[RAINSerializableClass]
public class PlayerCharacter : CustomAIElement
{
    //[RAINSerializableField]
    //private float _standardFloat = 1.2f;

    //[RAINSerializableField(Visibility = FieldVisibility.ShowAdvanced, ToolTip = "An advanced float")]
    //private float _advancedFloat = 4.5f;

    [RAINSerializableField]
    private GameObject m_Team;

    [RAINSerializableField]
    private int m_id;

    [RAINSerializableField]
    private GameObject m_posInTeam;

    private AIRig m_TeamAI;

    private TeamAI m_TeamAIElement;

    private Transform m_initialParent;

    public GameObject team
    {
        set { m_Team = value;
            InitializeTeamVar();
        }
        get { return m_Team; }
    }

    public int characterId
    {
        set { m_id = value; }
        get { return m_id; }
    }

    public GameObject positionInTeam
    {
        set { m_posInTeam = value; }
        get { return m_posInTeam; }
    }

    public override void AIInit()
    {
        base.AIInit();
        InitializeTeamVar();
        AI.WorkingMemory.SetItem<GameObject>("Position_In_Team", null);
        m_initialParent = AI.Body.transform.parent;
        // This is equivilent to an Awake call
    }

    public override void Sense()
    {
        base.Sense();

        if(AI.WorkingMemory.GetItem<GameObject>("Position_In_Team") == null)
        {
            AI.WorkingMemory.SetItem("Position_In_Team", m_posInTeam);
        }
        //Debug.Log(AI.Motor.IsAtMoveTarget);
        RAIN.Motion.MoveLookTarget a = new RAIN.Motion.MoveLookTarget();
        a.ObjectTarget = m_posInTeam;
        //Debug.Log("Arrived:" + AI.Motor.IsAt(a));

        if(AI.WorkingMemory.GetItem<bool>("isInTeam") != true)
        {

            if (AI.Motor.IsAt(a))
            {
                AI.WorkingMemory.SetItem("isInTeam", true);
                m_TeamAIElement.JoinTeam(m_id);
                AI.Body.transform.parent = m_Team.transform;
            }
            else
            {
                AI.WorkingMemory.SetItem("isInTeam", false);
            }
        }

        if (AI.WorkingMemory.GetItem<GameObject>("target") != null)
        {
            AI.Body.transform.parent = m_initialParent;
            AI.WorkingMemory.SetItem("isInTeam", false);
            m_TeamAIElement.LeaveTeam(m_id);
        }

        Debug.Log(AI.WorkingMemory.GetItem<GameObject>("target"));
        
    }

    private void InitializeTeamVar()
    {
        if (m_Team != null)
        {
            m_TeamAI = m_Team.GetComponentInChildren<AIRig>();
            m_TeamAIElement = m_TeamAI.AI.GetCustomElement<TeamAI>();
        }
    }
}