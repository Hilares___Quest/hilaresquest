﻿using RAIN.Core;
using RAIN.Serialization;
using UnityEngine;

[RAINSerializableClass]
public class NetworkChanger : CustomAIElement
{
    [RAINSerializableField]
    protected string _networkNameValue;

    [RAINSerializableField]
    private WaypointNetworkManager Manager;

    virtual public void SetNetwork(GameObject newNetwork)
    {

    }

    public override void AIInit()
    {
        base.AIInit();
        if (Manager != null)
            Manager.AddEvent(SetNetwork);
        else
            Debug.LogWarning("Manager is not set");
    }
}